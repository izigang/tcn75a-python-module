from flask import Flask, render_template, request, redirect, url_for
from TCN75ASIMUL import TCN75A as Capteur

app = Flask(__name__)

capteur = None
erreur = ""

# Définition de la route pour la page web
@app.route('/')
def launcher():
    global capteur
    global erreur

    if capteur != None: # Retour sur le launcher après initialisation
        return afficher_temperature()
    
    try: # Premier lancement
        capteur = Capteur(debug_mode = True)
    except Exception as e: # Configuration capteur impossible
        erreur = str(e)
        capteur = None
        return erreur_interface()
    else:
        return afficher_temperature()


@app.route("/interface")
def afficher_temperature():
    if capteur == None:
        return erreur_interface()
    return render_template('page.html', temperature=capteur.get_temperature(), mode_veille= capteur.is_stand_by())


@app.route("/erreur")
def erreur_interface():
    if capteur!=None:
        return afficher_temperature()
    return render_template('erreur.html', erreur = erreur)


# Définition de la route pour activer le mode veille
@app.route('/mode_veille', methods=['POST'])
def mode_veille():
    action = request.form['action']
    if action == 'activer':
        capteur.awake()
        print("requete activation")
    elif action == 'desactiver':
        capteur.stand_by()
        print("requete mise en veille")
    elif action == "one-shot":
        capteur.one_shot()
        print("requete one-shot")

    return afficher_temperature()


@app.route('/temperature')
def get_temperature():
    if capteur != None:
        return str(capteur.get_temperature())
    return '0'


@app.route("/parametrage_avance", methods=["POST"])
def parametrage():
    global capteur
    global e
    if capteur != None:
        return afficher_temperature()
    bus = request.form["bus"]
    adresse = request.form["bus"]
    
    try:
        capteur = Capteur(addr=adresse, bus=bus)
    except Exception as e:
        erreur = str(e)
        capteur = None
        return erreur_interface()
    else:
        return afficher_temperature()

# Lancement du serveur
if __name__ == '__main__':
    app.run(debug=True)