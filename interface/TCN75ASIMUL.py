import time

"""
Module pour la gestion de capteur de température numerique TCN75A. 
Compatible Raspberry PI 3B
"""

TEMP_REG = 0x00 #Adresse du registre de température (R)
CONFIG_REG = 0x01 #Adresse du registre de configuration (RW)
DEFAULT_CONFIG = 0x01100000 #Configuration par défaut du capteur

class TCN75A:
	"""
	Classe pour la gestion d'un capteur TCN75A
	"""
	def __init__(self,addr = 0x48, bus = 1, debug_mode = False):
		"""
		Initialisation du capteur

		Paramètres: 
			addr :         adresse I2C du capteur (0x48 par défaut)
			bus :          bus I2C du capteur (1 par défaut)
			debug_mode :   Affichage des informations de débuggage (True par défaut)
		
		Actions:
			Ouvre le bus I2C
			Applique la configuration par défaut
		"""
		self.addr = addr
		self.bus = bus
		self.debug_mode = debug_mode

		if(self.debug_mode):
			raise Exception("Erreur : Communication sur le bus impossible. Assurez vous d'avoir activé l'interface i2c avant de continuer")

		self.set_config(DEFAULT_CONFIG)

	def print_debug(self, msg):
		"""
		Affichage de débuggage: Affiche msg si mode débuggage activé
		
		Paramètre:
			msg : message à afficher
		"""
		if self.debug_mode:
			print(msg)

	def get_config(self):
		"""
		Récuperation de la configuration actuelle du capteur
		
		Retour:
		Valeur du registre de configuration du capteur (sur 1 octet)
		"""
		return self.config


	def set_config(self, config):
		"""
		Mise à jour du registre de configuration du capteur
		
		Paramètre:
			config : Nouvelle valeur du registre de configuration (sur 1 octet)
		"""
		self.config = config


	def is_stand_by(self):
		"""
		Verification de veille du capteur

		Retour : True si le capteur est en veille, False sinon
		"""
		return self.get_config() % 2


	def get_temperature(self):
		"""
		Lecture de la température

		Retour : Valeur du registre de température du capteur (sur 2 octets)
		"""
		return 20

	def stand_by(self):
		"""
		Mise en veille du capteur
		"""
		if (self.is_stand_by()):
			self.print_debug("Le capteur est déjà en veille")
		else:
			self.set_config(self.get_config() | 0b00000001)
	
	def awake(self):
		"""
		Sortie de veille du capteur
		"""
		if not(self.is_stand_by()):
			self.print_debug("Le capteur est déjà actif")
		else:
			self.set_config(self.get_config() & 0b1111110)
	
	def one_shot(self):
		"""
		Déclenchement d'une capture instantanée de la température en veille
		"""
		if not(self.is_stand_by()):
			self.print_debug("Le capteur n'est pas en veille")
		self.set_config(self.get_config() | 0b10000000)
		time.sleep(0.003)



if __name__ == "__main__":
	sensor = TCN75A()
	print("Stand by : ", sensor.is_stand_by(), "check 0")
	print("Temperature : ", sensor.get_temperature(),)
	sensor.stand_by()
	print("Stand by : ", sensor.is_stand_by(), "check 1")
	sensor.one_shot()
	print("Temperature : ", sensor.get_temperature())
	sensor.awake()
	print("Stand by : ", sensor.is_stand_by(), "check 0")
