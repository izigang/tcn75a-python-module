# AUTHOR : PAUL NAUTRE - THOMAS EKINDY
# DATE : 15/03/2023
# TITLE : TCN75A module

"""
DESCRIPTION :

Module pour la gestion de capteur de température numerique TCN75A. 
Compatible Raspberry PI 3B
"""
import time

TEMP_REG = 0x00 #Adresse du registre de température (R)
CONFIG_REG = 0x01 #Adresse du registre de configuration (RW)
DEFAULT_CONFIG = 0x01100000 #Configuration par défaut du capteur (précision max, mode actif)

class TCN75A:
	"""
	Classe pour la gestion d'un capteur TCN75A
	"""
	def __init__(self,addr = 0x48, bus = 1, debug_mode = True):
		"""
		Initialisation du capteur

		Paramètres: 
			addr :         adresse I2C du capteur (0x48 par défaut)
			bus :          bus I2C du capteur (1 par défaut)
			debug_mode :   Affichage des informations de débuggage (True par défaut)
		
		Actions:
			Ouvre le bus I2C
			Applique la configuration par défaut
		"""

		self.addr = addr
		self.bus = bus
		self.debug_mode = debug_mode

		try:
			import smbus
		except ModuleNotFoundError as e:
			self.print_debug("Erreur : Impossible d'importer la librairie smbus. Assurez vous de l'avoir installé avant de continuer")
			exit(-1)

		try:
			self.bus = smbus.SMBus(self.bus)
			self.set_config(DEFAULT_CONFIG)
		except Exception as e:
			print_debug("Erreur : Communication sur le bus impossible. Assurez vous d'avoir activé l'interface i2c avant de continuer")
			exit(-1)

	def print_debug(self, msg):
		"""
		Affichage de débuggage: Affiche msg si mode débuggage activé
		
		Paramètre:
			msg : message à afficher
		"""
		if self.debug_mode:
			print(msg)


	def read_i2c(self, reg):
		"""
		Lecture sur le bus I2C à l'adresse du capteur
		
		Paramètre:
		reg : Registre à lire (sur 1 octet)

		Retour:
		Valeur du registre
		"""
		v = 0
		try:
			v = self.bus.read_byte_data(self.addr, reg)
		except Exception as e:
			self.print_debug("Erreur lors de l'écriture : vérifiez l'adresses et le bus de votre capteur")
			self.print_debug(e)

		return v


	def write_i2c(self, reg, data):
		"""
		Ecriture sur le bus I2C à l'adresse du capteur
		
		Paramètre:
		reg :  Registre à écrire (sur 1 octet)
		data : Information à écrire sur le registre
		"""
		try:
			self.bus.write_byte_data(self.addr, reg, data)
		except Exception as e:
			self.print_debug("Erreur lors de la lecture : vérifiez l'adresses et le bus de votre capteur")
			self.print_debug(e)


	def get_config(self):
		"""
		Récuperation de la configuration actuelle du capteur
		
		Retour:
		Valeur du registre de configuration du capteur (sur 1 octet)
		"""
		return self.read_i2c(CONFIG_REG)


	def set_config(self, config):
		"""
		Mise à jour du registre de configuration du capteur
		
		Paramètre:
			config : Nouvelle valeur du registre de configuration (sur 1 octet)
		"""
		self.write_i2c(CONFIG_REG,config)


	def is_stand_by(self):
		"""
		Verification de veille du capteur

		Retour : True si le capteur est en veille, False sinon
		"""
		return self.get_config() % 2 #test de parité sur la config car le bit de veille est le bit poids faible de la config


	def get_temperature(self):
		"""
		Lecture de la température

		Retour : Valeur du registre de température du capteur (sur 2 octets)
		"""
		return self.read_i2c(TEMP_REG)

	def stand_by(self):
		"""
		Mise en veille du capteur
		"""
		if (self.is_stand_by()):
			self.print_debug("Le capteur est déjà en veille")
		else:
			self.set_config(self.get_config() | 0b00000001)
	
	def awake(self):
		"""
		Sortie de veille du capteur
		"""
		if not(self.is_stand_by()):
			self.print_debug("Le capteur est déjà actif")
		else:
			self.set_config(self.get_config() & 0b1111110)
	
	def one_shot(self):
		"""
		Déclenchement d'une capture instantanée de la température en veille
		"""
		if not(self.is_stand_by()):
			self.print_debug("Le capteur n'est pas en veille")
		self.set_config(self.get_config() | 0b10000000)
		time.sleep(0.003)



if __name__ == "__main__":
	help(TCN75A)
	print("##################### DEMONSTRATION ###########################")
	print("Initialisation d'un capteur")
	sensor = TCN75A()
	print("Veille : ", sensor.is_stand_by(), "check 0")
	print("Température : ", sensor.get_temperature(),)
	sensor.stand_by()
	print("Veille : ", sensor.is_stand_by(), "check 1")
	sensor.one_shot()
	print("Température : ", sensor.get_temperature())
	sensor.awake()
	print("Stand by : ", sensor.is_stand_by(), "check 0")
